# Blockchain

Just a little experiment with Blockchain

Inspired by

* [https://stackoverflow.com/questions/51567462/typescript-class-extends-a-generic-type](https://stackoverflow.com/questions/51567462/typescript-class-extends-a-generic-type)
* [https://medium.com/coinmonks/understanding-and-building-your-own-tiny-blockchain-in-javascript-a16f2137cfec](https://medium.com/coinmonks/understanding-and-building-your-own-tiny-blockchain-in-javascript-a16f2137cfec)

## Getting Started

### Installing

Using npm:

```CMD
npm i --save blockchain
```

### Using

in TypeScript:

```TypeScript
import { Blockchain, IBlock } from 'blockchain';

// Creating an instance
const blockchain: IBlockchain<any> = new Blockchain('MyBlockchain');

// Add an item
blockchain.add({ id: 0, name: 'something', ... })

// Gets the first item
const firstItem: IBlock<any> = blockchain.first();

// Gets all items of the Blockchain
const allItems: IBlock<any>[] = blockchain.all();

// Validate item
const isValid: boolean = blockchain.valid(firstItem);
```

## Authors

* [thomas3577](https://thomas3577@bitbucket.org/thomas3577/blockchain.git)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
