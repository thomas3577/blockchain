import { v4 as uuid } from 'uuid';
import { BinaryToTextEncoding, createHmac } from 'crypto';

export const invalidDataError = 'The block data must be an object and cannot be null or undefined';

/**
 * Block extend interface
 * @export
 * @interface IBlockExtend
 */
interface IBlockExtend {
  /**
   * Gets the index of the block
   * @type {number}
   * @memberof BlockExtend
   */
  index: number;

  /**
   * Gets the previous hash
   * @type {string}
   * @memberof BlockExtend
   */
  hash: string;

  /**
   * Gets the previous hash
   * @type {string}
   * @memberof BlockExtend
   */
  previousHash: string;

  /**
   * Gets true if the block is valid
   * @type {Function}
   * @memberof BlockExtend
   */
  valid: () => boolean;

  /**
   * Gets the object as JSON
   * @returns {string}
   * @memberof IBlockExtend
   */
  toJson: () => string;
}

/**
 * Block extend class
 * @export
 * @class BlockExtend
 * @implements {IBlockExtend<T>}
 * @template T
 */
class BlockExtend<T extends Record<string, any>> implements IBlockExtend {
  /**
   * Gets the hash
   * @type {string}
   * @memberof BlockExtend
   */
  public readonly hash: string = null;

  /**
   * Gets true if the block is valid
   * @type {Function}
   * @memberof BlockExtend
   */
  public readonly valid: () => boolean = null;

  /**
   * Gets the object as JSON
   * @returns {string}
   * @memberof IBlockExtend
   */
  public readonly toJson: () => string = null;

  /**
   * Creates an instance of BlockExtend.
   * @param {number} index
   * @param {T} data
   * @param {string} [previousHash=null]
   * @param {IOptions} [options={ algorithm: 'sha256', encoding: 'base64' }]
   * @memberof BlockExtend
   */
  constructor(
    public readonly index: number,
    data: T,
    public readonly previousHash: string = null,
    options: IOptions = { algorithm: 'sha256', encoding: 'base64' }
  ) {
    const prototype: any = { ...BlockExtend.prototype };

    const createHash = (): string => {
      const secret: string = Object.keys(data)
        .filter(key => Object.prototype.hasOwnProperty.call(data, key))
        .map(key => data[key])
        .join();

      const hash: string = createHmac(options.algorithm, secret).digest(options.encoding);

      return hash;
    };

    Object.assign(prototype, Object.getPrototypeOf(data));
    Object.setPrototypeOf(this, prototype);
    Object.assign(this, data);

    this.hash = createHash();
    this.valid = (): boolean => this.hash === createHash();
    this.toJson = (): string => JSON.stringify(this, (key: string, value: any) => key.startsWith('_') ? undefined : value);
  }
}

type Block<T extends Record<string, any>> = BlockExtend<T> & T;

/**
 * Genesis block data interface
 * @interface IGenesisBlockData
 */
interface IGenesisBlockData {
  identifier: 'GenesisBlock';
  created: Date;
}

export type IBlock<T extends Record<string, any>> = IBlockExtend & T;

export const Block: new <T extends Record<string, any>>(index: number, data: T, prevHash: string, options: IOptions) => IBlock<T> = BlockExtend as any;

/**
 * Blockchain interface
 * @export
 * @interface IBlockchain
 * @template T
 */
export interface IBlockchain<T extends Record<string, any>> {
  /**
   * Id of the Blockchain (Readonly)
   * @type {string}
   * @memberof IBlockchain
   */
  id: string;

  /**
   * Name of the Blockchain
   * @type {string}
   * @memberof IBlockchain
   */
  name: string;

  /**
   * Adds a new block to the chain
   * @param {T} data
   * @returns {IBlock<T>}
   * @memberof IBlockchain
   */
  add(data: T): IBlock<T>;

  /**
   * Gets the genesis block
   * @type {IBlock<IGenesisBlockData>}
   * @memberof IBlockchain
   */
  genesis: IBlock<IGenesisBlockData>;

  /**
    * gest the first block of the chain
    * @type {IBlock<T>}
    * @memberof IBlockchain
    */
  first: IBlock<T>;

  /**
   * Gets the last block of the chain
   * @type {IBlock<T>}
   * @memberof IBlockchain
   */
  last: IBlock<T>;

  /**
   * Gets an array of all blocks
   * @type {IBlock<T>[]}
   * @memberof IBlockchain
   */
  all: IBlock<T>[];

  /**
   * Gets true if the whole block chain is valid
   * @type {boolean}
   * @memberof IBlockchain
   */
  valid: boolean;
}

/**
 * Options interface
 * @export
 * @interface IOptions
 */
export interface IOptions {
  /**
   * Type of algorithm to encrypt the hash
   * @type {string}
   * @memberof IOptions
   */
  algorithm?: string;

  /**
   * Type of encoding to encrypt the hash
   * @type {string}
   * @memberof IOptions
   */
  encoding?: BinaryToTextEncoding;
}

/**
 * Blockchain class
 * @export
 * @class Blockchain
 * @implements {IBlockchain<T>}
 * @template T
 */
export class Blockchain<T extends Record<string, any>> implements IBlockchain<T> {
  private readonly _id = uuid();
  private readonly _created = new Date();
  private readonly _chain: IBlock<T>[];

  /**
   * Creates an instance of Blockchain.
   * @param {string} _name
   * @param {IOptions} [_options]
   * @memberof Blockchain
   */
  constructor(
    private readonly _name: string,
    private readonly _options?: IOptions
  ) {
    this._chain = [this._createGenesisBlock(this._created, this._options) as any];
  }

  public add(data: T): IBlock<T> {
    if (!data || typeof data === 'string' || typeof data === 'boolean' || typeof data === 'number') {
      throw new Error(invalidDataError);
    }

    const lastBlock: IBlock<T> = this.last;
    const block: IBlock<T> = new Block<T>(lastBlock.index + 1, data, lastBlock.hash, this._options);

    this._chain.push(block);

    return block;
  }

  public get id(): string {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get genesis(): IBlock<IGenesisBlockData> {
    return this._chain[0] as IBlock<any> || null;
  }

  public get all(): IBlock<T>[] {
    return this._chain.slice(1);
  }

  public get first(): IBlock<T> {
    return this._chain[1] || null;
  }

  public get last(): IBlock<T> {
    return this._chain[this._chain.length - 1] || null;
  }

  public get valid(): boolean {
    return this._chain.every((_, index) => index < 1 || this._validateBlock(this._chain[index], this._chain[index - 1], this._chain[0]));
  }

  /**
   * Validates a block for its validity
   * @private
   * @param {IBlock<T>} block
   * @param {IBlock<T>} previousBlock
   * @param {IBlock<T>} genesisBlock
   * @return {*}  {boolean}
   * @memberof Blockchain
   */
  private _validateBlock(block: IBlock<T>, previousBlock: IBlock<T>, genesisBlock: IBlock<T>): boolean {
    return block !== undefined
      && block.valid()
      && block.previousHash === previousBlock?.hash
      && genesisBlock?.hash === this._createGenesisBlock(this._created, this._options).hash;
  }

  /**
   * Created the genesis block
   * @private
   * @param {Date} created
   * @param {IOptions} options
   * @return {*}  {IBlock<any>}
   * @memberof Blockchain
   */
  private _createGenesisBlock(created: Date, options: IOptions): IBlock<IGenesisBlockData> {
    const genesisBlockData: IGenesisBlockData = {
      created,
      identifier: 'GenesisBlock'
    };

    return new Block<IGenesisBlockData>(0, genesisBlockData, null, options);
  }
}
