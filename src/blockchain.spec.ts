import 'mocha';
import { expect } from 'chai';
import { Blockchain, IBlockchain, invalidDataError } from './blockchain';
import { createHmac } from 'crypto';

interface IUser {
  user: string;
  password: string;
  firstName?: string;
  lastName?: string;
  email: string;
  created: Date;
}

describe('blockchain.ts', () => {
  describe('constructor()', () => {
    it('if name is null', () => {
      const name = null;
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it('if name is undefined', () => {
      const name = undefined;
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it(`if name is ''`, () => {
      const name = '';
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it(`if name is a object`, () => {
      const name = { id: 123 };
      const blockchain: IBlockchain<IUser> = new Blockchain((name as unknown) as string);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it(`if name is a array`, () => {
      const name = [1, 2, 3];
      const blockchain: IBlockchain<IUser> = new Blockchain((name as unknown) as string);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it(`if name is a boolean (true)`, () => {
      const name = true;
      const blockchain: IBlockchain<IUser> = new Blockchain((name as unknown) as string);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it(`if name is a boolean (false)`, () => {
      const name = false;
      const blockchain: IBlockchain<IUser> = new Blockchain((name as unknown) as string);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });
  });

  describe('add()', () => {
    it('if no item added', () => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(0);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
    });

    it('if undefined added', (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      try {
        blockchain.add(undefined);
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it('if null added', (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      try {
        blockchain.add(null);
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it(`if '' added`, (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<unknown> = new Blockchain(name);

      try {
        blockchain.add('');
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it(`if 'test' item added`, (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<unknown> = new Blockchain(name);

      try {
        blockchain.add('test');
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it(`if boolean (true) item added`, (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<unknown> = new Blockchain(name);

      try {
        blockchain.add(true);
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it(`if boolean (false) item added`, (done) => {
      const name = 'MyBlockchain';
      const blockchain: IBlockchain<any> = new Blockchain(name);

      try {
        blockchain.add(false);
      } catch (error) {
        expect(error.message).to.be.equal(invalidDataError);
        done();
      }
    });

    it('if one item added', () => {
      const name = 'MyBlockchain';
      const user: IUser = {
        user: 'Thomas3577',
        password: createHmac('sha256', 'MyPassword').digest('base64'),
        firstName: 'Thomas',
        lastName: 'Huber',
        email: 'thomas3577@gmx.net',
        created: new Date()
      };

      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      blockchain.add(user);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have one item').to.be.equal(1);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
      expect(blockchain.first.index, 'Index of first item should be 1').to.be.equal(1);
      expect(blockchain.first.user, 'Username should be not change').to.be.equal('Thomas3577');
      expect(blockchain.first.password, 'Password should be not change').to.be.equal(createHmac('sha256', 'MyPassword').digest('base64'));
      expect(blockchain.last.index, 'Index of first item should be 1').to.be.equal(1);
      expect(blockchain.last.user, 'Username should be not change').to.be.equal('Thomas3577');
      expect(blockchain.last.password, 'Password should be not change').to.be.equal(createHmac('sha256', 'MyPassword').digest('base64'));
      expect(blockchain.valid, 'Block should be valid').to.be.true;
    });

    it('if two items added', () => {
      const name = 'MyBlockchain';
      const user1: IUser = {
        user: 'A',
        password: 'B',
        firstName: 'C',
        lastName: 'D',
        email: 'E',
        created: new Date()
      };

      const user2: IUser = {
        user: 'a',
        password: 'b',
        firstName: 'c',
        lastName: 'd',
        email: 'e',
        created: new Date()
      };

      const blockchain: IBlockchain<IUser> = new Blockchain(name);

      blockchain.add(user1);
      blockchain.add(user2);

      expect(blockchain.id, 'Id should not be null').to.be.not.null;
      expect(blockchain.name, 'Name not be not changed').to.be.equal(name);
      expect(blockchain.all.length, 'Should have two item').to.be.equal(2);
      expect(blockchain.genesis.index, 'Index of genesis should be 0').to.be.equal(0);
      expect(blockchain.first.index, 'Index of first item should be 1').to.be.equal(1);
      expect(blockchain.first.user, 'Username should be not change').to.be.equal('A');
      expect(blockchain.first.password, 'Password should be not change').to.be.equal('B');
      expect(blockchain.last.index, 'Index of first item should be 1').to.be.equal(2);
      expect(blockchain.last.user, 'Username should be not change').to.be.equal('a');
      expect(blockchain.last.password, 'Password should be not change').to.be.equal('b');
      expect(blockchain.valid, 'Block should be valid').to.be.true;
    });
  });
});
